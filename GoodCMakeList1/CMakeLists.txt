cmake_minimum_required(VERSION 2.8)
project(scratch)

set(CMAKE_CXX_FLAGS "-std=c++11")

set(OBJECT_FILES
	../src/object.h
	../src/object.cpp
)

set(TEST_FILES
	../src/testdata.cpp
	../src/testdata.h
)

add_library(TestDataLib ${TEST_FILES})

add_executable(main.exe ${OBJECT_FILES} ../src/main.cpp)
target_link_libraries(main.exe TestDataLib)

