EDIT: I've since solved the problem below with help from /r/learnprogramming.
      This is actually just an example of something commonly known as the Static
      Initialization Order Fiasco, or SIOF. I've fixed the problem by putting the
      map in a function.

  There's some conceptual puzzle piece about libraries and/or compiling I'm not
getting. I don't understand why the incorrect CMakeLists don't work, and the
others do. The main problem is that in some configurations the values of
strong enums declared and defined in object.h are not visible in testdata.cpp,
despite object.h being included in testdata.h.

  *All libraries are static*

  BadCMakeList1 says main.exe depends on all the source files, without
libraries. This fails
  BadCMakeList2 turns object.h and object.cpp into a library. main.exe
then depends on testdata.h, testdata.cpp, and links to object-library. This fails.
  GoodCMakeList1 turns testdata.h and testdata.cpp into a library. main.exe
then depends on object.h, object.cpp, and links to testdata-library. This succeeds.
  GoodCMakeList2 turns testdata.h and testdata.cpp into a library and turns
object.h and object.cpp into another library. main.exe then links to these two
libraries. This succeeds.

  To test it for yourself just run init.sh from the root directory, this
runs 'cmake .' and 'make -s' inside each of the subdirectories. Then 'run.sh' should
run 'main.exe' in each subdirectory. testdata.cpp will try to create an Object using a
StrongEnum as an argument to std::map.at(). Failures should show an out_of_range
exception to std::map and print the tried input. Success won't display
any output.

What is it about turning the testdata into a library and linking it that makes it succeed?
