#include "object.h"

Object::Object(const StrongEnum& se) : strongEnum(se)
{
  try {
    value = getStrongEnumValue(se);
  }
  catch(std::out_of_range) {
    std::cout << "Caught std::out_of_range exception when trying to access\n"
              << "std::map<StrongEnum, int>.at(" << static_cast<int>(se) << ")"
              << std::endl;
  }
}

int getStrongEnumValue(const StrongEnum& strongEnum) {
  static const std::map<StrongEnum, int> strongEnumValues {
    {StrongEnum::ENUM1, 1},
    {StrongEnum::ENUM2, 2},
    {StrongEnum::ENUM3, 3}
  };

  return strongEnumValues.at(strongEnum);
}

