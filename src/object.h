#ifndef OBJECT
#define OBJECT

#include <iostream>
#include <exception>
#include <map>

enum class StrongEnum : int {
  ENUM1,
  ENUM2,
  ENUM3
};

int getStrongEnumValue(const StrongEnum& strongEnum);

class Object {
public:
  StrongEnum strongEnum;
  int value;

  Object(const StrongEnum& t);
};

#endif

